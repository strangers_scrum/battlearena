﻿using UnityEngine;
using System.Collections;
using NUnit.Framework;

[TestFixture]
public class BattleTest : MonoBehaviour {
	
	[Test]
	public void Player_TakeDamage()
	{
		PlayerManager playmanager = new PlayerManager();
		playmanager.health = 100;
		playmanager.TakeDamage (50);
		Assert.Equals (playmanager.health, 50);
	}

	public void Player_TakeDamage_ThenDie()
	{
		PlayerManager playmanager = new PlayerManager();
		playmanager.health = 100;
		playmanager.TakeDamage (100);
		playmanager.Received (1).Die ();
	}
}
