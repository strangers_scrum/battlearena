﻿using UnityEngine;
using System.Collections;

public class BulletManager : Photon.MonoBehaviour
{

    #region LifeTime
    [Header("LifeTime")]
    public float lifeTime = 0.1f;
    public float curLife = 0;
    #endregion

    #region BulletStats
    [Header("BulletStats")]
    public int bulletDamage;
    public float bulletSpeed;
    #endregion
    // Use this for initialization
    void Start ()
    {

	}
	
    void Update()
    {
        if(curLife < lifeTime)
        {
            curLife += Time.deltaTime;
        }
        else
        {
            Destroy(this.gameObject);
            //GetComponent<PhotonView>().RPC("DestroyBullet", PhotonTargets.All);
        }
    }

    void OnCollisionEnter2D(Collision2D col)
    {

    }
}
