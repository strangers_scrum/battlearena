﻿using UnityEngine;
using System.Collections;

public class DeathZone : MonoBehaviour
{
    void OnCollisionEnter2D(Collision2D col)
    {
        GameObject collidedGo = col.gameObject;

        if(collidedGo.tag == "Player")
        {
            collidedGo.GetComponent<PlayerManager>().Die();
        }
    }
}
