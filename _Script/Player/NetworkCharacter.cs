﻿using UnityEngine;
using System.Collections;

public class NetworkCharacter : MonoBehaviour {

    private Vector2 realPosition = Vector3.zero;
    private Quaternion realRotation = Quaternion.identity;

    private PhotonView photonView;

    // Use this for initialization
    void Start ()
    {
        photonView = GetComponent<PhotonView>();
	}
	
	// Update is called once per frame
	void Update ()
    {
        if(!photonView.isMine)
        {
            transform.position = Vector3.Lerp(transform.position, realPosition, 10f * Time.deltaTime);
            transform.rotation = Quaternion.Lerp(transform.rotation, realRotation, 10f * Time.deltaTime);
        }
	}


    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.isWriting)
        {
            // Your Player
            stream.SendNext(transform.position);
            stream.SendNext(transform.rotation);
        }
        else if (stream.isReading)
        {
            // Other Player
            realPosition = (Vector3)stream.ReceiveNext();
            realRotation = (Quaternion)stream.ReceiveNext();
        }
    }
}
