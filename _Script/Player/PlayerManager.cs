﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PlayerManager : Photon.MonoBehaviour
{
    #region Ground
    [Header("GroundCollision")]
    public Transform groundCheck;
    public LayerMask ground;
    public float groundRadius;
    public bool isGrounded;
    #endregion

    #region Movement
    [Header("Movement")]
    public float move;
    public float speed;
    public float jumpForce;
    public bool isMovingRight;
    #endregion

    #region Components
    private Rigidbody2D rb2d;
    new private PhotonView photonView;
    #endregion

    #region Shoot
    public Transform shootSpot;
    #endregion

    #region PlayerStats
    [Header("PlayerStats")]
    public float health;
    private float maxHealth;
    public int kills;
    public int deaths;
    #endregion

    #region GUI
    [Header("GUI")]
    public GameObject gui;
    #endregion

    // Use this for initialization
    void Start ()
    {
        photonView = GetComponent<PhotonView>();

        if (!photonView.isMine)
        {
            GetComponent<NetworkCharacter>().enabled = false;
            this.enabled = false;
        }
        else
        {
            GameObject.Find("Camera").GetComponent<FollowPlayer>().player = this.transform;
        }
        rb2d = GetComponent<Rigidbody2D>();
        gui = GameObject.Find("GUI");
        maxHealth = health;

        UpdatePlayerScore();
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (GetComponent<PhotonView>().isMine)
        {
            Move();
            Jump();
            Shoot();
            FlipPlayer();
        }
    }

    void Move()
    {
        if (Input.GetAxis("Horizontal") != 0)
        {
            move = Input.GetAxis("Horizontal");
            rb2d.velocity = new Vector2(move * speed, rb2d.velocity.y);
            isMovingRight = (move > 0) ? true : false;
        }
    }
    
    void Jump()
    {
        if (Input.GetKeyDown(KeyCode.Space) && isGrounded)
        {
            rb2d.AddForce(new Vector2(rb2d.velocity.x, jumpForce));
        }

        isGrounded = Physics2D.OverlapCircle(groundCheck.position, groundRadius, ground);
    }

    void Shoot()
    {
        if (Input.GetMouseButtonDown(0))
        {
            float forceDirection = (isMovingRight) ? 1000 : -1000;

            photonView.RPC("SpawnBullet", PhotonTargets.All, forceDirection);
        }
    }

    void FlipPlayer()
    {
        this.transform.rotation = (isMovingRight) ? new Quaternion(0, 180, 0, 0) : new Quaternion(0, 0, 0, 0);
    }

    public void Die()
    {
        if (GetComponent<PhotonView>().isMine)
        {
            if(deaths < 99)
            {
                deaths++;
            }

            UpdatePlayerScore();
            GetComponent<PhotonView>().RPC("RespawnPlayer", PhotonTargets.All);
        }
    }

    void ResetPlayerStats()
    {
        if (GetComponent<PhotonView>().isMine)
        {
            health = maxHealth;
        }
    }

    void UpdatePlayerScore()
    {
        gui.GetComponentInChildren<Text>().text = "Eliminations: " + kills + " / " + "Deaths: " + deaths;
    }

    [PunRPC]
    void SpawnBullet(float pForceDirection)
    {
        GameObject bullet = Instantiate(Resources.Load("Bullet"), shootSpot.position, shootSpot.rotation) as GameObject;
        bullet.GetComponent<Rigidbody2D>().AddForce(new Vector2(pForceDirection, 0));
    }

    [PunRPC]
    void TakeDamage(int damage)
    {
        if (GetComponent<PhotonView>().isMine)
        {
            health -= damage;

            if (health <= 0)
            {
                health = 0;
                Die();
            }
        }
    }

    [PunRPC]
    void RespawnPlayer()
    {
        GameObject[] spawnPoints = GameObject.FindGameObjectsWithTag("SpawnPoint");

        int randomSpawnID = Random.Range(0, spawnPoints.Length - 1);

        ResetPlayerStats();

        transform.position = spawnPoints[randomSpawnID].transform.position;
    }
}
