# Description
BattleArena is a simple game developed with Unity. The goal is to kill as many other players with your gun as possible.

# Download
Download the latest release here: [Alpha version 0.1](https://bitbucket.org/strangers_scrum/battlearena/downloads/BattleArena%200.1.zip)
